## Configuración
Para el funcionamiento del plugin, hay que integrar el modulo de mobile_vision:

1. **git clone git@gitlab.com:norte70/ocr_android.git**

2. **cd ocr_android**

3. **cp -rf ocr_mobile ../tre3ce_app/platforms/android/**

3. Dentro del proyecto de ../tre3ce_app/platforms/android/ . Hacer los siguientes ajustes:

  1. Modificar el archivo AndroidManifest.xml en la linea 16 agregar:
     <activity android:name="com.tre3ce.ocr_mobile.OcrCaptureActivity" android:theme="@style/OCRMobileTheme" />

  2. Modificar el archivo ../tre3ce_app/platforms/android/cordova/lib/builders/GradleBuilder.js en la linea 101 

     fs.writeFileSync(path.join(this.root, 'settings.gradle'),
    '// GENERATED FILE - DO NOT EDIT\n' +
    'include ":"\n' + settingsGradlePaths.join('') +
    **'include ":ocr_mobile"'**);

  3. Agregar la entrada en el archivo build.gradle en la linea 257:
	**compile project(":ocr_mobile")**

4. Compilar el project de android: **phonegap build android**

5. phonegap plugin add https://gitlab.com/norte70/cordova-plugin-tre3ce-vision.git --save



