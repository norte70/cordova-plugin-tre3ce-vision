package com.nortesetenta.tre3ce;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import com.tre3ce.ocr_mobile.OcrCaptureActivity;

import android.util.Log;

/**
 * This class echoes a string called from JavaScript.
 */
public class Tre3ceVision extends CordovaPlugin {

    CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("readCredential")) {
            this.callbackContext = callbackContext;

            Intent intent = new Intent(cordova.getActivity().getApplicationContext(), OcrCaptureActivity.class);
            this.cordova.startActivityForResult((CordovaPlugin)this, intent, 0);
            return true;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && data != null){
            String[] credential = data.getStringArrayExtra("CREDENTIAL");

            if (credential.length > 0) {
                String jsonString = "{\"name\":\""+credential[0]+"\",\"last_name_pat\":\""+credential[1]+"\"," +
                        "\"last_name_mat\":\""+credential[2]+"\",\"elector_key\":\""+credential[3]+"\"," +
                        "\"section\":\""+credential[4]+"\",\"file\":\""+credential[5]+"\"}";

                this.callbackContext.success(jsonString);
            }else{
                this.callbackContext.error("Falló el escaneo");
            }
        }
    }
}
